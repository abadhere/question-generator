<!DOCTYPE html>
<html>

<head>
    <title>Quiz Site</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet" /> -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('assets/css/material-kit.min.css') }}" rel="stylesheet" type="text/css" >
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" >
	<meta name="csrf-token" content="{{ csrf_token() }}">
    
<style>
	.popup-btn{
    float: right;
    clear: both;
    margin-top: -60px;
    position: relative;
    margin-right: 15px;
    background: white;
    padding: 5px 10px;
    color: #33bf51;
    text-transform: uppercase;
    font-size: 14px;
    border: 1px solid silver;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
}
	</style>
    
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
</head>

<body style="background: none;">
    <nav class="navbar navbar-default" role="navigation-demo">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Galaxy Publication</a>
            </div>
        </div>
        <!-- /.container-->
    </nav>
    	<div class="select-option">
       <div class="container">
		 <div class="col-sm-12 dropdown-buttons">   
			<div class="col-sm-3 dropdown-button">           			
    		   <div class="section_1">
				
				 <select class="frm-field required" name="standard_id" id="standard">
                    <option value="">Select</option>
                    @foreach($standards as $stand)
                    <option value="{{$stand->id}}">{{$stand->standard_name}}</option>
                    @endforeach

                    </select>
			  </div>
			</div>
			<div class="col-sm-3 dropdown-button">           			
    		   <div class="section_1">
			   <select class="frm-field required" name="subject" id="subject">
			   

                    </select>
			  </div>
			</div>
    	   <div class="col-sm-3 dropdown-button">           			
    		  <div class="section_1">
				 <select class="frm-field required"  name="chapter_name" id="chapter">
					
				 </select>
			  </div>
			</div>
		    
			 <div class="clearfix"> </div>
		  </div> 
	<!-- 	  <div class="col-sm-2 submit_button"> 
		   	  <form>
		   	     <input type="submit" value="Search">
		   	  </form>
		   </div> -->
		   <div class="clearfix"> </div>
	     </div>
     </div>
  <div class="question-side">
     	<div class="container">
     		
            <div class="col-md-6 grid_right">
    

 <div class="ques">
                    <h2>Question Generator</h2>
                    <div class="list-q" id=quanswer>
                   
                  </div>
                </div>
      </div>
      <div class="col-md-6">
                <div class="select-ques ques1">
					<h2>Selected Questions</h2>
					<div><input type="text" placeholder="Enter Section">
					<input type="text" placeholder="Enter Section Title">
					<input type="button" value="Add"></div>
					<a href="#" class="popup-btn" data-toggle="modal" data-target="#exampleModal">Generate</a>
					
                    <div class="list-q pdfcontent" id="added">
                   
                  </div>
                </div>
            </div>
      <div class="clearfix"> </div>
     </div>
	</div>
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"><h5>Insert Paper Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <form class="form-inline" action="/action_page.php">
    <div class="form-group">
      <label for="email">School Name:</label>
      <input type="text" class="form-control" id="school"  name="school">
    </div>
    <div class="form-group">
      <label for="pwd">Marks:</label>
      <input type="text" class="form-control" id="marks" name="marks">
	</div>
	<div class="form-group">
      <label for="email">Paper Code:</label>
      <input type="text" class="form-control" id="code"  name="paper_code">
    </div>
	<div class="form-group">
      <label for="pwd">Title:</label>
      <input type="text" class="form-control" id="title"  name="title">
	</div>
	<div class="form-group">
      <label for="email">Hours</label>
      <input type="text" class="form-control" id="duration"  name="duration">
    </div>

    <div class="form-group">
      <label for="pwd">Year:</label>
      <input type="text" class="form-control" id="year" name="year">
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="generatepdf">Save</button>
      </div>
    </div>
  </div>
</div>
   
    <footer class="footer">
        <div class="container">
            <a class="footer-brand" href="#">General publication</a>
        </div>
    </footer>
</body>
<!--   Core JS Files   -->

<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/js/material.min.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/js/material-kit.js?v=1.1.0') }}"  type="text/javascript"></script>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="standard_id"]').on('change', function() {

		var standard_id = $(this).val();
		if(standard_id) {
		$.ajax({
		url: '/admin/subjects/ajax/'+encodeURI(standard_id),
		type: "GET",
		dataType: "json",
		success:function(data) {
		var html = "<option>Select</option>";
		

		for(var i=0;i<data.length;i++){
		
		
		html+="<option value="+data[i].id+">"+data[i].subject_name+"</option>";
	
		}
		$("#subject").html(html);
		}
		});
		}else{
		$('select[name="state"]').empty();
		}
		});



		$('select[name="subject"]').on('change', function() {

		var subject_id = $(this).val();
		if(subject_id) {
		$.ajax({
		url: '/admin/chapters/ajax/'+encodeURI(subject_id),
		type: "GET",
		dataType: "json",
		success:function(data) {
			var html = "<option>Select</option>";
		

			for(var i=0;i<data.length;i++){
		
				html+="<option value="+data[i].id+">"+data[i].chapter_name+"</option>";
		
			}
			$("#chapter").html(html);

			}
		});
		}else{
			$('select[name="state"]').empty();
		}
		});


		//for QUestion 
		$('select[name="chapter_name"]').on('change', function() {
		
		var chapter_id = $(this).val();
		if(chapter_id) {
		$.ajax({
		url: '/admin/questions/ajax/'+encodeURI(chapter_id),
		type: "GET",
		dataType: "json",
		success:function(data) {
		var html = "";
		for(var i=0;i<data.length;i++){
		

		html+="<div class='questions'><div class='q-1'><span><strong>Q.1 </strong>"+data[i].quest_name+"</span>";
		html+="<input type='hidden' class='question-text' value='"+data[i].quest_name+"'>";
		html+="<span><button class='add-question' value="+data[i].id+">Add</button></span></div>";
		html+="<div class='ans'><p><strong>Ans.</strong>"+data[i].answer+"</p></div></div>";

		}
		$("#quanswer").html(html);
		addQuestion();

		}

		});
		}else{
		$('select[name="state"]').empty();
		}
		});
		function addQuestion(){
			$(".add-question").on("click",function(e){
				e.stopImmediatePropagation();
				e.preventDefault();
				
				var ques = $(this).parent().parent().find(".question-text").val(); 
				var qno = $(".list-q").find(".q-1").length;
				var id = $(this).val();
				var html = "<div class='q-1 gq'><span id="+id+" class='qid'><strong>Q."+qno+"</strong> "+ques+"</span></div>";
				$(".pdfcontent").append(html);
				$('.qid').on("click",function(){
					
				alert($(this).attr('id'));

			});
			})
			
			
		}
		//generate Question
		$("#generatepdf").on("click",function(){
			//modal code to be added later
			debugger;
			var data  = {
				questions:fetchData(),
				standard:$('#standard option:selected').text(),
				subject:$('#subject option:selected').text(),
				chapter:$('#chapter option:selected').text(),
				school:$('#school').val(),
				title:$('#title').val(),
				duration:$('#duration').val(),
				marks:$('#marks').val(),
				paper_code:$('#code').val(),
				year:$('#year').val(),
			};
			$.ajax({
				url:"/insert",
				type:"POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: data,
				success:function(response){
					
				}
			})
		})
		function fetchData(){
			var ques = [];
			$(".pdfcontent").find(".gq").each(function(i){
				ques.push($(this).text());
			})
			return ques;
		}

	});
    </script>