<!DOCTYPE html>
<html>

<head>
    <title>Quiz Site</title>
    <title>Quiz Site</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet" /> -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <link href="{{ asset('assets/css/material-kit.min.css') }}" rel="stylesheet" type="text/css" >
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" >
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body style="background: none;">
<style>
/*.grid_right{
      box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);

}*/
.list-q .col-sm-12{
      margin-top: 10px;
    margin-bottom: 10px;
    border-bottom: 2px dotted silver;
}
.list-q h3{
   font-size: 16px;
   text-align: left;
   margin: 0;
    font-weight: 600;
    text-transform: capitalize;
}
.related{
    
    margin-top: 21px;
    margin-bottom: 10px;
    border-bottom: 2px dotted silver;

}

.list-q button {
    float: right;
    margin-top: -25px;
    border-radius: 50%;
    height: 40px;
    width: 40px;
    border: 1px solid #0c62ff;
    background: #0c62ff;
    color: white;

}
h4{
   font-size: 15px;
   margin: 0;
    font-weight: 600;
    text-transform: capitalize;
}
h5{
      text-align: left;
}

#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;}
#country-list li{padding: 10px; width: 236px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
#search{padding: 10px; border-radius:4px;}
#search-box2{padding: 10px; border-radius:4px;} 
</style>
    <nav class="navbar navbar-default" role="navigation-demo">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Galaxy Publication</a>
            </div>
        </div>
        <!-- /.container-->
    </nav>
    <div class="select-option">
            <div class="container">
              <div class="col-sm-12 dropdown-buttons">   
                 <div class="col-sm-3 dropdown-button">           			
                    <div class="section_1">
                     
                      <select class="frm-field required" name="standard_id" id="standard">
                         <option value="">Select</option>
                         @foreach($standards as $stand)
                         <option value="{{$stand->id}}">{{$stand->standard_name}}</option>
                         @endforeach
     
                         </select>
                   </div>
                 </div>
                 <div class="col-sm-3 dropdown-button">           			
                    <div class="section_1">
                    <select class="frm-field required" name="subject" id="subject">
                    
     
                         </select>
                   </div>
                 </div>
                <div class="col-sm-3 dropdown-button">           			
                   <div class="section_1">
                      <select class="frm-field required"  name="chapter_name" id="chapter">
                         
                      </select>
                   </div>
                 </div>
              
               </div> 
       
                <div class="clearfix"> </div>
              </div>
          </div>
  <!--   
            <div class="clearfix"> </div>
        </div>
    </div> -->
    <div class="question-side">
        <div class="container">
            <div class="row">
          <div class="search">
          <form action="" method="post"> 
            <input type="text" class="sub-search" id="search"  name="search" placeholder="Seach here..." required="">
            <input type="submit" value="">
            <div id="video-data"></div>
          </form>
        </div>
      </div>
        <div class="row">
           <div class="col-md-8 grid_right">

                <div class="ques">
                    <h2>Searched Results</h2>
                    <div class="list-q full-video col-md-12" id=videourl>
                     
      @foreach ($data as $standobj)   
      <div class="row">
          <div class="col-sm-12">

                             
    <div class="col-sm-4">
    <iframe width="200" height="120" src="{{ $standobj->url }}?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
    <div class="col-sm-7">
      <h3>{{ $standobj->title }}</h3>
      <!-- <h5>Here is going about video</h5> -->
      <button class="play" value="{{ $standobj->id }}"><i class="fa fa-play fa-2x" style="padding: 0px 8px;"></i></button>
    </div>
</div>
      </div>                      
    @endforeach
    </div>         
                </div>
            </div>
            <div class="col-md-4">
            
                <div class="select-ques ques1" style="
    display: none;
">
                    <h2>Related Videos</h2>
                    <div class="list-q">
                        <div class="video-list">
                            <ul>
                            @foreach ($related as $standobj)   
                                <li class="related">
    <iframe width="250" height="120" style="margin-top: 10px;" src="{{ $standobj->url }}?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    <h4 class="play"  data-idtest="">{{ $standobj->title }}</h4>
                    <button class="play" value="{{ $standobj->id }}">PLAY</button>

                                </li>    
                                @endforeach                            
                            </ul>
                            <!-- <button class="btn btn-block">loading More...</button> -->
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
           
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <a class="footer-brand" href="#">General publication</a>
        </div>
    </footer>
</body>
<!--   Core JS Files   -->

<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/js/material.min.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/js/material-kit.js?v=1.1.0') }}"  type="text/javascript"></script>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="standard_id"]').on('change', function() {

		var standard_id = $(this).val();
		if(standard_id) {
		$.ajax({
		url: '/admin/subjects/ajax/'+encodeURI(standard_id),
		type: "GET",
		dataType: "json",
        beforeSend: function(){
        // $("#chapter").css("background","#FFF url(img/frontend/LoaderIcon.gif) no-repeat 165px");
        $('#subject').prepend($('<option></option>').html('Loading...'));
        },
		success:function(data) {
		var html = "<option>Select</option>";
		

		for(var i=0;i<data.length;i++){
		
		
		html+="<option value="+data[i].id+">"+data[i].subject_name+"</option>";
	
		}
		$("#subject").html(html);
		}
		});
		}else{
		$('select[name="state"]').empty();
		}
		});



		$('select[name="subject"]').on('change', function() {

		var subject_id = $(this).val();
		if(subject_id) {
		$.ajax({
		url: '/admin/chapters/ajax/'+encodeURI(subject_id),
		type: "GET",
		dataType: "json",
        beforeSend: function(){
        // $("#chapter").css("background","#FFF url(img/frontend/LoaderIcon.gif) no-repeat 165px");
        $('#chapter').prepend($('<option></option>').html('Loading...'));

        },
		success:function(data) {
			var html = "<option>Select</option>";
		

			for(var i=0;i<data.length;i++){
		
				html+="<option value="+data[i].id+">"+data[i].chapter_name+"</option>";
		
			}
			$("#chapter").html(html);

			}
		});
		}else{
			$('select[name="state"]').empty();
		}
		});


		$('select[name="chapter_name"]').on('change', function() {
            var chapter_id = $(this).val();
            if(chapter_id) {
            $.ajax({
            url: '/admin/videos/ajax/'+encodeURI(chapter_id),
            type: "GET",
            dataType: "json",
            success:function(data) {
                debugger;
            var html = "";
            // for(var i=0;i<data.length;i++){
            html+='<iframe width="720" height="315" src="'+data[0].url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
            html+="<h4>'"+data[0].title+"'</h4>";
            $("#videourl").html(html);
            }
            });
            }else{
            $('select[name="state"]').empty();
            }
            });



//for Video

	$('.play').on('click', function() {
            
            var uid = $(this).val();
            
            if(uid) {
            $.ajax({
            url: '/admin/video/ajax/'+encodeURI(uid),
            type: "GET",
            dataType: "json",
            success:function(data) {
                
            var html = "";
            // for(var i=0;i<data.length;i++){
            html+='<iframe width="720" height="315" src="'+data[0].url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
            html+="<h4>'"+data[0].title+"'</h4>";
            $("#videourl").html(html);
            $('.ques1').show();

            }
            });
            }else{
            $('select[name="state"]').empty();
            }
            });

//Hide DIv
// $('.hide').load(function() {
//       $(this).hide();
//     });
// $(".hide").hide();


    // $('#search').on('keyup',function(){
    // var term=$(this).val();
    // $.ajax({
    // url : '/admin/search/ajax/'+encodeURI(term),    
    // type : 'get',
    // dataType: "json",
    // beforeSend: function(){
    // $("#search").css("background","#FFF url(images/LoaderIcon.gif) no-repeat 165px");
    // },
    // success:function(data){
    //     debugger;
    //     var html = "";

        
    //     for(var i=0;i<data.length;i++){

    //     html+="<ul id='country-list'><li onclick='selectTitle('Data entry executive');'>'"+data[0].title+"'</li>";
        
    //     }
    //     $("#video-data").append(html);

    // }
    // });
    // })  



		function fetchData(){
			var ques = [];
			$(".pdfcontent").find(".gq").each(function(i){
				ques.push($(this).text());
		})
			return ques;
		}

      
        });

    
</script>
