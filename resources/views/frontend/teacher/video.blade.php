<!DOCTYPE html>
<html>

<head>
    <title>Quiz Site</title>
    <title>Quiz Site</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet" /> -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('assets/css/material-kit.min.css') }}" rel="stylesheet" type="text/css" >
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" >
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body style="background: none;">
<style>


#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;}
#country-list li{padding: 10px; width: 236px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
#search{padding: 10px; border-radius:4px;}
#search-box2{padding: 10px; border-radius:4px;} 
</style>
    <nav class="navbar navbar-default" role="navigation-demo">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Galaxy Publication</a>
            </div>
        </div>
        <!-- /.container-->
    </nav>
    <div class="select-option">
            <div class="container">
              <div class="col-sm-12 dropdown-buttons">   
                 <div class="col-sm-3 dropdown-button">           			
                    <div class="section_1">
                     
                      <select class="frm-field required" name="standard_id" id="standard">
                         <option value="">Select</option>
                         @foreach($standards as $stand)
                         <option value="{{$stand->id}}">{{$stand->standard_name}}</option>
                         @endforeach
     
                         </select>
                   </div>
                 </div>
                 <div class="col-sm-3 dropdown-button">           			
                    <div class="section_1">
                    <select class="frm-field required" name="subject" id="subject">
                    
     
                         </select>
                   </div>
                 </div>
                <div class="col-sm-3 dropdown-button">           			
                   <div class="section_1">
                      <select class="frm-field required"  name="chapter_name" id="chapter">
                         
                      </select>
                   </div>
                 </div>
                 
                  <div class="clearfix"> </div>
               </div> 
       
                <div class="clearfix"> </div>
              </div>
          </div>
    
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="question-side">
        <div class="container">
            <div class="row">
          <div class="search">
          <form action="" method="post"> 
          {{ csrf_field() }}

            <input type="text" class="sub-search" id="search"  name="search" placeholder="Seach here..." required="">
            <input type="submit" value="">
            <div id="video-data"></div>
          <!-- </form> -->
        </div>
      </div>
        <div class="row">
           <div class="col-md-8 grid_right">

                <div class="ques">
                    <h2>Student Videos</h2>
                    <div class="list-q full-video" id=videourl>
                        
                           
                        
                        
                    </div>
                </div>
            </div>
           
            <div class="clearfix"> </div>
        </div>
           
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <a class="footer-brand" href="#">General publication</a>
        </div>
    </footer>
</body>
<!--   Core JS Files   -->

<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/js/material.min.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/js/material-kit.js?v=1.1.0') }}"  type="text/javascript"></script>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="standard_id"]').on('change', function() {

		var standard_id = $(this).val();
		if(standard_id) {
		$.ajax({
		url: '/admin/subjects/ajax/'+encodeURI(standard_id),
		type: "GET",
		dataType: "json",
		success:function(data) {
		var html = "<option>Select</option>";
		

		for(var i=0;i<data.length;i++){
		
		
		html+="<option value="+data[i].id+">"+data[i].subject_name+"</option>";
	
		}
		$("#subject").html(html);
		}
		});
		}else{
		$('select[name="state"]').empty();
		}
		});



		$('select[name="subject"]').on('change', function() {

		var subject_id = $(this).val();
		if(subject_id) {
		$.ajax({
		url: '/admin/chapters/ajax/'+encodeURI(subject_id),
		type: "GET",
		dataType: "json",
		success:function(data) {
			var html = "<option>Select</option>";
		

			for(var i=0;i<data.length;i++){
		
				html+="<option value="+data[i].id+">"+data[i].chapter_name+"</option>";
		
			}
			$("#chapter").html(html);

			}
		});
		}else{
			$('select[name="state"]').empty();
		}
		});


		//for Question 
		$('select[name="chapter_name"]').on('change', function() {
            
		var chapter_id = $(this).val();
		if(chapter_id) {
		$.ajax({
		url: '/admin/videos/ajax/'+encodeURI(chapter_id),
		type: "GET",
		dataType: "json",
		success:function(data) {
            debugger;
		var html = "";
        // for(var i=0;i<data.length;i++){
        html+='<iframe width="560" height="315" src="'+data[0].url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
		html+="<h4>'"+data[0].title+"'</h4>";
		$("#videourl").html(html);
		}
		});
		}else{
		$('select[name="state"]').empty();
		}
		});


    // $('#search').on('keyup',function(){
    // var term=$(this).val();
    // $.ajax({
    // url : '/admin/search/ajax/'+encodeURI(term),    
    // type : 'get',
    // dataType: "json",
    // beforeSend: function(){
    // $("#search").css("background","#FFF url(images/LoaderIcon.gif) no-repeat 165px");
    // },
    // success:function(data){
    //     debugger;
    //     var html = "";

        
    //     for(var i=0;i<data.length;i++){

    //     html+="<ul id='country-list'><li onclick='selectTitle('Data entry executive');'>'"+data[0].title+"'</li>";
        
    //     }
    //     $("#video-data").append(html);

    // }
    // });
    // })  



		function fetchData(){
			var ques = [];
			$(".pdfcontent").find(".gq").each(function(i){
				ques.push($(this).text());
		})
			return ques;
		}

      
        });

    
</script>
