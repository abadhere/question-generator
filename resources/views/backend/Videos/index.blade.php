@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.videos.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.videos.management') }} <small class="text-muted">{{ __('labels.backend.access.videos.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.videos.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.access.videos.table.sno') }}</th>
                            <th>{{ __('labels.backend.access.videos.table.videos_title') }}</th>
                            <th>{{ __('labels.backend.access.videos.table.videos_name') }}</th>
                            
                            
                            <th>{{ __('labels.backend.access.videos.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($videos as $standobj)
                            <tr>
                                <td>{{ $standobj->id }}</td>
                                <td>{{ $standobj->title }}</td>
                                <td>{{ $standobj->url }}</td>
                                <td>{{ $standobj->updated_at->diffForHumans() }}</td>
                                <td>{!! $standobj->action_buttons !!}</td>
                            </tr>
                        @endforeach;
                        
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $videos->total() !!} {{ trans_choice('labels.backend.access.videos.table.total', $videos->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $videos->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
