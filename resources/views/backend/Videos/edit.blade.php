@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.videos.management') . ' | ' . __('labels.backend.access.videos.edit'))

<!-- @section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection -->

@section('content')



{{ html()->modelForm($video, 'PATCH', route('admin.videos.update', $video->id))->class('form-horizontal')->open() }}
<div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.videos.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.videos.edit') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.videos.videos_name'))->class('col-md-2 form-control-label')->for('videos_name') }}

                            <div class="col-md-10">
                                {{ html()->text('title')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.videos.videos_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.videos.url'))->class('col-md-2 form-control-label')->for('url') }}

                            <div class="col-md-10">
                                {{ html()->text('url')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.videos.url'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="col-sm-4">
                {{ html()->label(__('validation.attributes.backend.access.chapter.chapter_name'))->class('col-sm-6 form-control-label')->for('mobile') }}
                    <div class="form-group">
                    <select class="form-control" name="chapter_id">
                    <option value="">Select</option>
                                 @foreach($chapters as $stand)
                                 
                                 <option value="{{$stand->id}}" {{$stand->id==$uniq_chapter->id?"selected":""}}>{{$stand->chapter_name}}</option>
                                 @endforeach
              

                    </select>
                </div><!--col-->
                </div>

    

                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.videos.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection