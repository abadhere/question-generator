@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.videos.management') . ' | ' . __('labels.backend.access.videos.create'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.videos.store'))->class('form-horizontal')->open() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.videos.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.videos.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.videos.videos_name'))->class('col-md-2 form-control-label')->for('videos_name') }}

                            <div class="col-md-10">
                                {{ html()->text('title')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.videos.videos_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            </div>
                            <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.videos.url'))->class('col-md-2 form-control-label')->for('answer') }}

                            <div class="col-md-10">
                                {{ html()->text('url')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.videos.url'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            </div>
                <div class="col-sm-4">
                {{ html()->label(__('validation.attributes.backend.access.standard.standard_name'))->class('col-sm-6 form-control-label')->for('mobile') }}
                    <div class="form-group">
                    <select class="form-control" name="standard_id" id="standard">
                    <option value="">Select</option>
                    @foreach($standards as $stand)
                    <option value="{{$stand->id}}">{{$stand->standard_name}}</option>
                    @endforeach

                    </select>
                </div><!--col-->
                </div>
                <div class="col-sm-4">
                {{ html()->label(__('validation.attributes.backend.access.subject.subject_name'))->class('col-sm-6 form-control-label')->for('mobile') }}
                    <div class="form-group">
                    <select class="form-control" name="subject" id="subject">
                    

                    </select>
                </div><!--col-->
                </div>

                 <div class="col-sm-4">
                {{ html()->label(__('validation.attributes.backend.access.chapter.chapter_name'))->class('col-sm-6 form-control-label')->for('mobile') }}
                    <div class="form-group">
                    <select class="form-control" name="chapter_id" id="chapter">
                    

                    </select>
                </div><!--col-->
                </div>

                         

                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.videos.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
    $('select[name="standard_id"]').on('change', function() {
        
        var standard_id = $(this).val();
            if(standard_id) {
            $.ajax({
                url: '/admin/subjects/ajax/'+encodeURI(standard_id),
                type: "GET",
                dataType: "json",
                success:function(data) {
                    var html = "<option>Select</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value="+data[i].id+">"+data[i].subject_name+"</option>";
                    }
                    $("#subject").html(html);
                }
            });
            }else{
            $('select[name="state"]').empty();
              }
           });



            $('select[name="subject"]').on('change', function() {
        
        var subject_id = $(this).val();
            if(subject_id) {
            $.ajax({
                url: '/admin/chapters/ajax/'+encodeURI(subject_id),
                type: "GET",
                dataType: "json",
                success:function(data) {
                    var html = "<option>Select</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value="+data[i].id+">"+data[i].chapter_name+"</option>";
                    }
                    $("#chapter").html(html);
                }
            });
            }else{
            $('select[name="state"]').empty();
              }
           });


//For videos
            $('select[name="chapter_id"]').on('change', function() {
        
        var chapter_id = $(this).val();
            if(chapter_id) {
            $.ajax({
                url: '/admin/videoss/ajax/'+encodeURI(chapter_id),
                type: "GET",
                dataType: "json",
                success:function(data) {
                    var html = "";
                    for(var i=0;i<data.length;i++){
                        html+="<option value="+data[i].id+">"+data[i].chapter_name+"</option>";
                    }
                    $("#chapter").html(html);
                }
            });
            }else{
            $('select[name="state"]').empty();
              }
           });
        });
    </script>