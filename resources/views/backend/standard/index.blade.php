@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.standard.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.standard.management') }} <small class="text-muted">{{ __('labels.backend.access.standard.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.standard.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.access.standard.table.sno') }}</th>
                            <th>{{ __('labels.backend.access.standard.table.standard_name') }}</th>
                            
                            
                            <th>{{ __('labels.backend.access.standard.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($standard as $standobj)
                            <tr>
                                <td>{{ $standobj->id }}</td>
                                <td>{{ $standobj->standard_name }}</td>
                                
                               
                                <td>{{ $standobj->updated_at->diffForHumans() }}</td>
                                <td>{!! $standobj->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $standard->total() !!} {{ trans_choice('labels.backend.access.standard.table.total', $standard->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $standard->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
