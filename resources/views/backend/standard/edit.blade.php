@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.standard.management') . ' | ' . __('labels.backend.access.standard.edit'))

<!-- @section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection -->

@section('content')



{{ html()->modelForm($standard, 'PATCH', route('admin.standard.update', $standard->id))->class('form-horizontal')->open() }}
<div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.standard.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.standard.edit') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.standard.standard_name'))->class('col-md-2 form-control-label')->for('standard_name') }}

                            <div class="col-md-10">
                                {{ html()->text('standard_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.standard.standard_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->

                       

    

                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.standard.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection