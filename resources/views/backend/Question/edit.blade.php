@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.question.management') . ' | ' . __('labels.backend.access.question.edit'))

<!-- @section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection -->

@section('content')



{{ html()->modelForm($question, 'PATCH', route('admin.question.update', $question->id))->class('form-horizontal')->open() }}
<div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.question.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.question.edit') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.question.question_name'))->class('col-md-2 form-control-label')->for('question_name') }}

                            <div class="col-md-10">
                                {{ html()->text('quest_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.question.question_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="col-sm-4">
                {{ html()->label(__('validation.attributes.backend.access.chapter.chapter_name'))->class('col-sm-6 form-control-label')->for('mobile') }}
                    <div class="form-group">
                    <select class="form-control" name="chapter_id">
                    <option value="">Select</option>
                                 @foreach($chapters as $stand)
                                 
                                 <option value="{{$stand->id}}" {{$stand->id==$uniq_chapter->id?"selected":""}}>{{$stand->chapter_name}}</option>
                                 @endforeach
              

                    </select>
                </div><!--col-->
                </div>

<div class="col-sm-4">
                {{ html()->label(__('validation.attributes.backend.access.chapter.chapter_name'))->class('col-sm-6 form-control-label')->for('mobile') }}
                    <div class="form-group">
                    <select class="form-control" name="chapter_id">
                    <option value="">Select</option>
                                 @foreach($standards as $stand)
                                 
                                 <option value="{{$stand->id}}" {{$stand->id==$uniq_standard->id?"selected":""}}>{{$stand->standard_name}}</option>
                                 @endforeach
              

                    </select>
                </div><!--col-->
                </div>
    

                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.question.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection