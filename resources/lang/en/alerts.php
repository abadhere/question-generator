<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'The role was successfully created.',
            'deleted' => 'The role was successfully deleted.',
            'updated' => 'The role was successfully updated.',
        ],

        'standard' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The user was successfully confirmed.',
            'created'             => 'The standard was successfully created.',
            'deleted'             => 'The standard was successfully deleted.',
            'deleted_permanently' => 'The standard was deleted permanently.',
            'restored'            => 'The user was successfully restored.',
            'session_cleared'      => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The standard was successfully updated.',
            'updated_password'    => "The standard's password was successfully updated.",
        ],
        'subject' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The subject was successfully confirmed.',
            'created'             => 'The subject was successfully created.',
            'deleted'             => 'The subject was successfully deleted.',
            'deleted_permanently' => 'The subject was deleted permanently.',
            'restored'            => 'The subject was successfully restored.',
            'session_cleared'      => "The subject's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The subject was successfully updated.',
            'updated_password'    => "The subject's password was successfully updated.",
        ],
        'topic' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The topic was successfully confirmed.',
            'created'             => 'The topic was successfully created.',
            'deleted'             => 'The topic was successfully deleted.',
            'deleted_permanently' => 'The subject was deleted permanently.',
            'restored'            => 'The subject was successfully restored.',
            'session_cleared'      => "The topic's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The topic was successfully updated.',
            'updated_password'    => "The topic's password was successfully updated.",
        ],

        'chapter' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The chapter was successfully confirmed.',
            'created'             => 'The chapter was successfully created.',
            'deleted'             => 'The chapter was successfully deleted.',
            'deleted_permanently' => 'The chapter was deleted permanently.',
            'restored'            => 'The chapter was successfully restored.',
            'session_cleared'      => "The chapter's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The chapter was successfully updated.',
            'updated_password'    => "The chapter's password was successfully updated.",
        ],
        'question' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The question was successfully confirmed.',
            'created'             => 'The question was successfully created.',
            'deleted'             => 'The question was successfully deleted.',
            'deleted_permanently' => 'The question was deleted permanently.',
            'restored'            => 'The question was successfully restored.',
            'session_cleared'      => "The chapter's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The question was successfully updated.',
            'updated_password'    => "The question's password was successfully updated.",
        ],
        'videos' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The video was successfully confirmed.',
            'created'             => 'The video was successfully created.',
            'deleted'             => 'The video was successfully deleted.',
            'deleted_permanently' => 'The video was deleted permanently.',
            'restored'            => 'The video was successfully restored.',
            'session_cleared'      => "The chapter's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The video was successfully updated.',
            'updated_password'    => "The question's password was successfully updated.",
        ],
        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The user was successfully confirmed.',
            'created'             => 'The user was successfully created.',
            'deleted'             => 'The user was successfully deleted.',
            'deleted_permanently' => 'The user was deleted permanently.',
            'restored'            => 'The user was successfully restored.',
            'session_cleared'      => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The user was successfully updated.',
            'updated_password'    => "The user's password was successfully updated.",
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
    ],
];
