<?php

namespace App\Events\Backend\Videos;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserUpdated.
 */
class VideosUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $videos;

    /**
     * @param $Subject
     */
    public function __construct($videos)
    {
        $this->videos = $videos;
    }
}
