<?php

namespace App\Events\Backend\Videos;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class VideosDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $videos;

    /**
     * @param $user
     */
    public function __construct($videos)
    {
        $this->Videos = $videos;
    }
}
