<?php

namespace App\Events\Backend\Videos;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserCreated.
 */
class VideosCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $videos;

    /**
     * @param $user
     */
    public function __construct($videos)
    {
        $this->videos = $videos;
    }
}
