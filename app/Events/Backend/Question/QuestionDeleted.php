<?php

namespace App\Events\Backend\Topic;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class TopicDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $topic;

    /**
     * @param $user
     */
    public function __construct($topic)
    {
        $this->topic = $topic;
    }
}
