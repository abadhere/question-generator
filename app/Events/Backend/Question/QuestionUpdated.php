<?php

namespace App\Events\Backend\Topic;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserUpdated.
 */
class TopicUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $topic;

    /**
     * @param $Subject
     */
    public function __construct($topic)
    {
        $this->topic = $topic;
    }
}
