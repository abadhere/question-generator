<?php

namespace App\Events\Backend\Question;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserCreated.
 */
class QuestionCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $question;

    /**
     * @param $user
     */
    public function __construct($question)
    {
        $this->question = $question;
    }
}
