<?php

namespace App\Events\Backend\Chapter;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class ChapterDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $chapter;

    /**
     * @param $user
     */
    public function __construct($chapter)
    {
        $this->chapter = $chapter;
    }
}
