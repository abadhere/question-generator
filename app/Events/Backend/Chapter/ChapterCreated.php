<?php

namespace App\Events\Backend\Chapter;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserCreated.
 */
class ChapterCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $chapter;

    /**
     * @param $user
     */
    public function __construct($chapter)
    {
        $this->chapter = $chapter;
    }
}
