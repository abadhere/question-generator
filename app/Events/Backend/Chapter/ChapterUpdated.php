<?php

namespace App\Events\Backend\Chapter;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserUpdated.
 */
class ChapterUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $chapter;

    /**
     * @param $Subject
     */
    public function __construct($chapter)
    {
        $this->chaptert = $chapter;
    }
}
