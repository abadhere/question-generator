<?php

namespace App\Events\Backend\Subject;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserUpdated.
 */
class SubjectUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $subject;

    /**
     * @param $Subject
     */
    public function __construct($subject)
    {
        $this->subject = $subject;
    }
}
