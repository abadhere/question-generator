<?php

namespace App\Events\Backend\Subject;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class SubjectDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $subject;

    /**
     * @param $user
     */
    public function __construct($subject)
    {
        $this->subject = $subject;
    }
}
