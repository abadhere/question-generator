<?php

namespace App\Events\Backend\Standard;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class StandardDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $standard;

    /**
     * @param $user
     */
    public function __construct($standard)
    {
        $this->standard = $standard;
    }
}
