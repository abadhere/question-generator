<?php

namespace App\Events\Backend\Standard;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserUpdated.
 */
class StandardUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $standard;

    /**
     * @param $standard
     */
    public function __construct($standard)
    {
        $this->standard = $standard;
    }
}
