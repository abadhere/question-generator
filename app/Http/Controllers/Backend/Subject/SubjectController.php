<?php

namespace App\Http\Controllers\Backend\Subject;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject\Subject;
use App\Models\Standard\Standard;
use App\Http\Requests\Backend\Subject\StoreSubjectRequest;
use App\Http\Requests\Backend\Subject\UpdateSubjectRequest;
use App\Http\Requests\Backend\Subject\ManageSubjectRequest;
use App\Repositories\Backend\Subject\SubjectRepository;
use App\Repositories\Backend\Standard\StandardRepository;
use App\Events\Backend\Subject\SubjectCreated;
use App\Events\Backend\Subject\SubjectUpdated;
use App\Events\Backend\Subject\SubjectDeleted;
use DB;
class SubjectController extends Controller
{


    protected $subjectRepository;
    protected $submodel;
    public function __construct(SubjectRepository $subjectRepository, Standard $standard)
    {
        $this->submodel = $standard;
        $this->subjectRepository = $subjectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Subject $subject)
    {
       
      
      return view('backend.subject.index')
      ->withSubject($this->subjectRepository->getActivePaginated(25, 'id', 'asc'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(StandardRepository $standard, SubjectRepository $subject)
    {
        $standards=$standard->get();
        $subjects=$subject->get();

        
        return view('backend.subject.create',compact('standards','subjects'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubjectRequest $request)
    {
        
        // $this->subjectRepository->create($request->all());
        $this->subjectRepository->create($request->only(
            
            'subject_name',
            'standard_id'
            )
        );
        return redirect()->route('admin.subject.index')->withFlashSuccess(__('alerts.backend.subject.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
            //   return "<h1>Hello World!</h1>";

      return view('backend.subject.index')
      ->withSubject($this->SubjectRepository->getActivePaginated(25, 'id', 'asc'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject,Standard $standard)
    {
        
        $uniq_standard=$subject->standard()->first();
        $standards=$standard->get();

        
        return view('backend.subject.edit',compact('standards','subject','uniq_standard'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Subject $subject,UpdateSubjectRequest $request)
    {
        $this->subjectRepository->update($subject, $request->only(
            'subject_name',
            'standard_id'

        ));

        return redirect()->route('admin.subject.index')->withFlashSuccess(__('alerts.backend.subject.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        $this->subjectRepository->deleteById($subject->id);

        // event(new CustomerDeleted($customer));

        return redirect()->route('admin.subject.index')->withFlashSuccess(__('alerts.backend.subject.deleted'));
    }
}
