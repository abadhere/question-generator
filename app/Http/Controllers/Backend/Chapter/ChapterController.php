<?php

namespace App\Http\Controllers\Backend\Chapter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chapter\Chapter;
use App\Models\Subject\Subject;
use App\Http\Requests\Backend\Chapter\StoreChapterRequest;
use App\Http\Requests\Backend\Chapter\UpdateChapterRequest;
use App\Http\Requests\Backend\Chapter\ManageChapterRequest;
use App\Repositories\Backend\Subject\SubjectRepository;
use App\Repositories\Backend\Standard\StandardRepository;
use App\Repositories\Backend\Chapter\ChapterRepository;
use App\Events\Backend\Chapter\ChapterCreated;
use App\Events\Backend\Chapter\ChapterUpdated;
use App\Events\Backend\Chapter\ChapterDeleted;


class ChapterController extends Controller
{
    protected $chapterRepository;
    protected $submodel;
    public function __construct(ChapterRepository $chapterRepository)
    {
        // $this->submodel = $standard;
        $this->chapterRepository = $chapterRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Subject $subject,Chapter $chapter)
    {
        
          $chapters = $this->chapterRepository->getActivePaginated(25, 'id', 'asc');    
           return view('backend.chapter.index',compact('chapters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Subject $subject, ChapterRepository $chapter)
    {
        
        $subjects=$subject->get();   
        $chapters=$this->chapterRepository->getActivePaginated(25, 'id', 'asc');
           return view('backend.chapter.create',compact('subjects','chapters'));       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChapterRequest $request)
    {
        
        // $this->subjectRepository->create($request->all());
        $this->chapterRepository->create($request->only(
            
            'chapter_name',
            'subject_id'
            
            )
        );
        return redirect()->route('admin.chapter.index')->withFlashSuccess(__('alerts.backend.subject.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
            //   return "<h1>Hello World!</h1>";

      return view('backend.Chapter.index')
      ->withSubject($this->topicRepository->getActivePaginated(25, 'id', 'asc'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject,Chapter $chapter)
    {
        $uniq_subject=$chapter->subject()->first();
        $subjects=$subject->get();
        return view('backend.chapter.edit', compact('uniq_subject','chapter','subjects'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Chapter $chapter,UpdateChapterRequest $request)
    {
        $this->chapterRepository->update($chapter, $request->only(
            'chapter_name',
            'subject_id'
            
        ));

        return redirect()->route('admin.chapter.index')->withFlashSuccess(__('alerts.backend.chapter.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chapter $chapter)
    {
        $this->chapterRepository->deleteById($chapter->id);

        // event(new CustomerDeleted($customer));

        return redirect()->route('admin.chapter.index')->withFlashSuccess(__('alerts.backend.chapter.deleted'));
    }
}
