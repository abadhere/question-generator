<?php

namespace App\Http\Controllers\Backend\Standard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Standard\Standard;
use App\Repositories\Backend\Standard\StandardRepository;
use App\Http\Requests\Backend\Standard\StoreStandardRequest;
use App\Http\Requests\Backend\Standard\UpdateStandardRequest;
use App\Http\Requests\Backend\Standard\ManageStandardRequest;
use App\Events\Backend\Standard\StandardCreated;
use App\Events\Backend\Standard\StandardUpdated;
use App\Events\Backend\Standard\StandardDeleted;

class StandardController extends Controller
{


    protected $standardRepository;
    
    public function __construct(StandardRepository $standardRepository)
    {
        $this->standardRepository = $standardRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // return "<h1>Hello World!</h1>";

      return view('backend.standard.index')
      ->withStandard($this->standardRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.standard.create')
        ->withStandard($this->standardRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStandardRequest $request)
    {
        
        // $this->standardRepository->create($request->all());
        $this->standardRepository->create($request->only(
            'standard_name'
            )
        );
        return redirect()->route('admin.standard.index')->withFlashSuccess(__('alerts.backend.standard.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
            //   return "<h1>Hello World!</h1>";

      return view('backend.standard.index')
      ->withStandard($this->standardRepository->getActivePaginated(25, 'id', 'asc'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Standard $standard)
    {
        return view('backend.standard.edit')->withStandard($standard);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Standard $standard,UpdateStandardRequest $request)
    {
        $this->standardRepository->update($standard, $request->only(
            'standard_name'
        ));

        return redirect()->route('admin.standard.index')->withFlashSuccess(__('alerts.backend.standard.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Standard $standard)
    {
        $this->standardRepository->deleteById($standard->id);

        // event(new CustomerDeleted($customer));

        return redirect()->route('admin.standard.index')->withFlashSuccess(__('alerts.backend.standard.deleted'));
    }
}
