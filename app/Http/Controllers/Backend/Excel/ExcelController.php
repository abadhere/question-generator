<?php

namespace App\Http\Controllers\Backend\Excel;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Models\Standard\Standard;
use App\Models\Subject\Subject;
use App\Models\Chapter\Chapter;
use App\Models\Question\Question;
//use App\Repositories\Backend\Standard\StandardRepository;
 use App\Http\Requests\Backend\Excel\StoreExcelRequest;
 use App\Http\Requests\Backend\Excel\ManageExcelRequest;
//use App\Events\Backend\Standard\StandardCreated;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Excel;
use File;
use Illuminate\Support\Facades\Input;

class ExcelController extends Controller
{
    protected $standard;
    protected $subject;
    protected $chapter;
    protected $question;
    public function  __construct(Standard $standard,Subject $subject,Chapter $chapter,Question $question)
    {
        $this->standard = $standard;
        $this->subject = $subject;
        $this->chapter = $chapter;
        $this->question = $question;

    }

    public function create()
    {
        return view('backend.excel.create');
    }

    public function store(Request $request)
    {
        if($request->hasFile('import_file')){
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $recordMap = [];
                $currentStandard = null;
                $currentChapter = null;
                $currentSubject = null;
                $currentQuestion = null;
                $sid = null;
                $subjectId=null;
                $chapterId = null;
                foreach ($reader->toArray() as $key => $row) {

                    //check if current standard exists in db and get its ID
                     if($currentStandard != $row['standard_name']){
                        $standard = $this->standard->where('standard_name','=',$row['standard_name'])->first();
                        if($standard == null){
                            $data['standard_name'] = $row['standard_name'];
                            $standard = $this->standard->create($data);
                        }
                        $currentStandard = $standard->standard_name;
                        $sid= $standard->id;
                    }
                    

                               //check if current subject exists in db
                    if($currentSubject != $row['subject_name']){
                       $subject = $this->subject->where('standard_id','=',$sid)->where("subject_name","=",$row['subject_name'])->first();
                         if($subject == null){
                            $data['standard_id'] = $sid;
                            $data['subject_name'] = $row['subject_name'];
                             $subject = $this->subject->create($data);
                         }
                        $currentSubject = $subject->subject_name;
                        $subjectId= $subject->id;
                    } 
                        
                                //check if current chapter exists in db
                    if($currentChapter != $row['chapter_name']){
                    $chapter = $this->chapter->where('subject_id','=',$subjectId)->where('chapter_name','=',$row['chapter_name'])->first();
                        if($chapter == null){
                            $data['subject_id'] = $subjectId;
                            $data['chapter_name'] = $row['chapter_name'];
                            $chapter = $this->chapter->create($data);
                        }
                        $currentChapter = $chapter->chapter_name;
                        $chapterId= $chapter->id;    
                    } 
                        
                              //check if current Question exists in db
                      
                                $data['chapter_id'] = $chapterId;
                                $data['quest_name'] = $row['quest_name'];
                                $data['answer'] = $row['answer'];
                                $question = $this->question->create($data);
                            
                            
                           
                        
                }
            });
        }

        Session::put('success', 'Your file successfully import in database!!!');

        return back();
    }



}
