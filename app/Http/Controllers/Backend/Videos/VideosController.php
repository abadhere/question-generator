<?php

namespace App\Http\Controllers\Backend\Videos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chapter\Chapter;
use App\Models\Videos\Videos;
use App\Models\Standard\Standard;
use App\Models\Subject\Subject;
use App\Http\Requests\Backend\Videos\StoreVideosRequest;
use App\Http\Requests\Backend\Videos\UpdateVideosRequest;
use App\Http\Requests\Backend\Videos\ManageVideosRequest;
use App\Repositories\Backend\Question\QuestionRepository;
use App\Repositories\Backend\Videos\VideosRepository;
use App\Events\Backend\Videos\VideosCreated;
use App\Events\Backend\Videos\VideosUpdated;
use App\Events\Backend\Videos\VideosDeleted;
use DB;

class VideosController extends Controller
{

    
    public function __construct(VideosRepository $videosRepository)
    {
        
        $this->videosRepository = $videosRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Videos $video)
    {   
        $videos = $this->videosRepository->getActivePaginated(25, 'id', 'asc');    
        
           return view('backend.videos.index',compact('videos'));
        
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Chapter $chapter, Standard $standard,Subject $subject)
    {
        
        $chapters=$chapter->get();
        $standards=$standard->get();
        $subjects=$subject->get();
        $videos=$this->videosRepository->getActivePaginated(25, 'id', 'asc');
        
           return view('backend.videos.create',compact('standards','subjects','chapters','videos'));
           


        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideosRequest $request)
    {
        
        // $this->subjectRepository->create($request->all());
        $this->videosRepository->create($request->only(
            
            'title',
            'url',
            'chapter_id'
            
            )
        );
        return redirect()->route('admin.videos.index')->withFlashSuccess(__('alerts.backend.videos.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
            //   return "<h1>Hello World!</h1>";

      return view('backend.question.index')
      ->withQuestion($this->questionRepository->getActivePaginated(25, 'id', 'asc'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Videos $video,Chapter $chapter)
    {

        $uniq_chapter=$video->chapter()->first();
        $chapters=$chapter->get();
        
        return view('backend.videos.edit', compact('chapters','uniq_chapter','video'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Videos $video,UpdatevideosRequest $request)
    {
        $this->videosRepository->update($video, $request->only(
            'title',
            'url',
            'chapter_id'
            
        ));

        return redirect()->route('admin.videos.index')->withFlashSuccess(__('alerts.backend.videos.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Videos $video)
    {
        $this->videosRepository->deleteById($video->id);

        // event(new CustomerDeleted($customer));

        return redirect()->route('admin.videos.index')->withFlashSuccess(__('alerts.backend.videos.deleted'));
    }

    public function subjectsdata($standard_id, Subject $subject){
        $subjects_data=$subject->where('standard_id','=',$standard_id)->get()->toJson();
       
        return $subjects_data;
    }

    public function chaptersdata($subject_id, Chapter $chapter){
        $chapters_data=$chapter->where('subject_id','=',$subject_id)->get()->toJson();
       
        return $chapters_data;
    }

    public function questionsdata($chapter_id, Question $question){
        $questions_data=$question->where('chapter_id','=',$chapter_id)->get()->toJson();
        return $questions_data;
    }

    public function qdata($q_id, Question $question){
        $data=$question->where('id','=',$q_id)->get()->toJson();
        return $data;
    }

    public function insertdata(Request $request){
        $id = DB::table('paper_infos')->insertGetId(
            ['standard' => $stand_text,
             'subject' => $subj_text,
             'chapter' => $chap_text,
             'school' => $school,
             'marks' => $marks,
             'paper_code' => $code,
             'year' => $year,]
        );

    foreach($question_text as $key => $value)
    {
        $lastinsert=DB::table('users')->insert([
        ['question' => $question_text[$key],
        'p_id' => $id[$key]]
        ]);
    }
    return response()->json(201);
    }


}
