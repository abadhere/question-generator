<?php

namespace App\Http\Controllers\Frontend\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;


class ApiController extends Controller
{

    
    public function __construct()
    {
        
        // $this->questionRepository = $questionRepository;
    }


//Method for insert paper Data

public function test(){
    return "<h1>Hello World</h1>";
}

    public function insertdata(Request $request){
        $pid = DB::table('paper_informations')->insertGetId(
            ['standard'=>$request->standard,
            'subject'=>$request->subject,
            'chapter'=>$request->chapter,
            'school'=>$request->school_name,
            'marks'=>$request->marks,
            'paper_code'=>$request->paper_code,
            'title'=>$request->title,
            'duration'=>$request->duration,
            'year'=>$request->year,]
        );


        DB::table('sections')->insertGetId(
            ['p_id'=>$request->p_id,
             'section_text'=>$request->section_text,
            'section_heading'=>$request->section_heading,]
        );
        $sids = DB::table('sections')
        ->select('id')
        ->where('p_id', '=', $pid)->get();

        $questions=$request->questions;
        for($i = 0; $i < sizeof($questions); $i++)
    {
       
        $lastinsert=DB::table('question_texts')->insert([
        'questions' => $questions[$i],
        's_id' => $sids[$i],
        ]);
    }
    return response()->json("True");
    }

}