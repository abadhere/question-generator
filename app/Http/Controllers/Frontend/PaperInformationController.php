<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\PaperInformation\PaperInformation;
use App\Models\PaperInformation\Section;
use App\Models\PaperInformation\QuestionText;
use PDF;
use DB;
/**
 * Class ContactController.
 */
class PaperInformationController extends Controller
{


    public function downloadPDF($id,PaperInformation $q,Section $s){
        $questions = $q::find($id);
     $sectioninfo=Section::find($id)->get();
    $questiontext=QuestionText::find($id)->get();

       $pdf = PDF::loadView('frontend.pdf', compact('questions','sectioninfo','questiontext'));
        
       return $pdf->download('questionPaper.pdf');
        // return view('frontend.pdf', compact('questions','sectioninfo','questiontext'));

      } 

}