<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chapter\Chapter;
use App\Models\Question\Question;
use App\Models\Videos\Videos;
use App\Models\Standard\Standard;
use App\Models\Subject\Subject;
use App\Http\Requests\Backend\Question\StoreQuestionRequest;
use App\Http\Requests\Backend\Question\UpdateQuestionRequest;
use App\Http\Requests\Backend\Question\ManageQuestionRequest;
use App\Repositories\Frontend\Question\QuestionRepository;
use App\Repositories\Backend\Chapter\ChapterRepository;
use App\Events\Backend\Question\QuestionCreated;
use App\Events\Backend\Question\QuestionUpdated;
use App\Events\Backend\Question\QuestionDeleted;

class QuestionController extends Controller
{

    
    public function __construct(QuestionRepository $questionRepository)
    {
        
        $this->questionRepository = $questionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Question $question,Chapter $chapter)
    {
     
        $questions = $this->questionRepository->getActivePaginated(25, 'id', 'asc');
            
           return view('frontend.teacher.home',compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Chapter $chapter, Standard $standard,Subject $subject)
    {
        
        $chapters=$chapter->get();
        $standards=$standard->get();
        $subjects=$subject->get();
        $questions=$this->questionRepository->getActivePaginated(25, 'id', 'asc');
        
           return view('frontend.teacher.home',compact('standards','subjects','chapters','questions'));

    }


    public function search(Videos $video,Request $request,Chapter $chapter, Standard $standard,Subject $subject){
        $chapters=$chapter->get();
        $standards=$standard->get();
        $subjects=$subject->get();
        $questions=$this->questionRepository->getActivePaginated(25, 'id', 'asc');
        $term=$request->input('search');
        $data=$video->where('title','like','%'.$term.'%')->take(5)->get();
        $cid=$data[0]->chapter_id;
        $related=$video->where('chapter_id','=',$cid)->take(5)->get();

       
        return view('frontend.teacher.search',compact('standards','subjects','chapters','questions','data','related'));
    }


    public function video(Chapter $chapter, Standard $standard,Subject $subject)
    {
        
        $chapters=$chapter->get();
        $standards=$standard->get();
        $subjects=$subject->get();
        $questions=$this->questionRepository->getActivePaginated(25, 'id', 'asc');
        
           return view('frontend.teacher.video',compact('standards','subjects','chapters','questions'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQuestionRequest $request)
    {
        
        // $this->subjectRepository->create($request->all());
        $this->questionRepository->create($request->only(
            
            'quest_name',
            'chapter_id'
            
            )
        );
        return redirect()->route('admin.question.index')->withFlashSuccess(__('alerts.backend.question.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
            //   return "<h1>Hello World!</h1>";

      return view('backend.question.index')
      ->withQuestion($this->questionRepository->getActivePaginated(25, 'id', 'asc'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Chapter $chapter,Question $question,Subject $subject,Standard $standard)
    {
        $uniq_standard=$subject->standard()->first();
        $uniq_subject=$chapter->subject()->first();
        $uniq_chapter=$question->chapter()->first();
        $chapters=$subject->get();
        $chapters=$chapter->get();
        $chapters=$standard->get();
        $chapters=$chapter->get();
        return view('backend.question.edit', compact('question','chapters','uniq_chapter'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Question $question,UpdatequestionRequest $request)
    {
        $this->questionRepository->update($question, $request->only(
            'quest_name',
            'chapter_id'
            
        ));

        return redirect()->route('admin.question.index')->withFlashSuccess(__('alerts.backend.question.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $this->questionRepository->deleteById($question->id);

        // event(new CustomerDeleted($customer));

        return redirect()->route('admin.question.index')->withFlashSuccess(__('alerts.backend.question.deleted'));
    }

    public function subjectsdata($standard_id, Subject $subject){
        $subjects_data=$subject->where('standard_id','=',$standard_id)->get()->toJson();
       
        return $subjects_data;
    }

    public function chaptersdata($subject_id, Chapter $chapter){
        $chapters_data=$chapter->where('subject_id','=',$subject_id)->get()->toJson();
       
        return $chapters_data;
    }

    public function questionsdata($chapter_id, Question $question){
        $questions_data=$question->where('chapter_id','=',$chapter_id)->get()->toJson();
       
        return $questions_data;
    }

   

   
}
