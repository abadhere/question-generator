<?php

namespace App\Http\Requests\Backend\Chapter;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateSubjectRequest.
 */
class UpdateChapterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'chapter_name'     => 'required|max:191',
            'subject_id'     => 'required|max:191',
            
            
        ];
    }
}
