<?php
namespace App\Models\Standard;

use Illuminate\Database\Eloquent\Model;
use App\Models\Standard\Traits\Attribute\StandardAttribute;
/**
 * @property int $id
 * @property string $standard_name
 * @property string $created_at
 * @property string $updated_at
 * @property Subject[] $subjects
 */
class Standard extends Model
{
    /**
     * @var array
     */
use StandardAttribute;
    protected $fillable = ['standard_name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subjects()
    {
        return $this->hasMany('App\Models\Subject\Subject','standard_id');
    }
}

