<?php

namespace App\Models\Chapter;

use Illuminate\Database\Eloquent\Model;
use App\Models\Chapter\Traits\Attribute\ChapterAttribute;


/**
 * @property int $id
 * @property int $topic_id
 * @property string $chapter_name
 * @property string $created_at
 * @property string $updated_at
 * @property Topic $topic
 * @property Question[] $questions
 */
class Chapter extends Model
{
    use ChapterAttribute;

    /**
     * @var array
     */
    protected $fillable = ['subject_id', 'chapter_name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subject()
    {
        return $this->belongsTo('App\Models\Subject\Subject');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function question()
    {
        return $this->hasMany('App\Models\Question\Question');
    }
}

