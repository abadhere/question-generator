<?php
namespace App\Models\Question;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $chapter_id
 * @property string $quest_name
 * @property string $created_at
 * @property string $updated_at
 * @property Chapter $chapter
 */
class Question extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['chapter_id', 'quest_name','answer','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chapters()
    {
        return $this->belongsTo('App\Chapter\Chapter');
    }
}
