<?php
namespace App\Models\Videos;

use Illuminate\Database\Eloquent\Model;
use App\Models\Videos\Traits\Attribute\VideosAttribute;

/**
 * @property int $id
 * @property int $chapter_id
 * @property string $url
 * @property Chapter $chapter
 */
class Videos extends Model
{
    /**
     * @var array
     */
    use VideosAttribute;

    protected $fillable = ['chapter_id','title', 'url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chapter()
    {
        return $this->belongsTo('App\Models\Chapter\Chapter');
    }
}
