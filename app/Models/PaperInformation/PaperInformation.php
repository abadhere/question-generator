<?php

namespace App\Models\PaperInformation;
use App\Models\PaperInformation\Section;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $chapter_id
 * @property string $quest_name
 * @property string $created_at
 * @property string $updated_at
 * @property Chapter $chapter
 */
class PaperInformation extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['paper_code', 'subject','chapter','standard','year','school','marks','duration', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sections()
    {
        return $this->hasMany('App\Models\PaperInformation\Section','p_id');
    }


}
