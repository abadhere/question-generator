<?php

namespace App\Models\PaperInformation;
use App\Models\PaperInformation\PaperInfomation;
use App\Models\PaperInformation\QuestionText;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $chapter_id
 * @property string $quest_name
 * @property string $created_at
 * @property string $updated_at
 * @property Chapter $chapter
 */
class Section extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['p_id', 'section_text','section_heading', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paperinformations()
    {
        return $this->belongsTo('App\Models\PaperInformation\PaperInformation','id');
    }
    public function questiontexts()
    {
        return $this->hasMany('App\Models\PaperInformation\QuestionText','s_id');
    }

}