<?php

namespace App\Models\PaperInformation;
use App\Models\PaperInformation\Section;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $chapter_id
 * @property string $quest_name
 * @property string $created_at
 * @property string $updated_at
 * @property Chapter $chapter
 */
class QuestionText extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['s_id', 'questions', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sections()
    {
        return $this->belongsTo('App\Models\PaperInformation\Section','id');
    }

}
