<?php

namespace App\Models\Subject;

use Illuminate\Database\Eloquent\Model;
use App\Models\Subject\Traits\Attribute\SubjectAttribute;

/**
 * @property int $id
 * @property string $subject_name
 * @property string $created_at
 * @property string $updated_at
 * @property Topic[] $topics
 */
class Subject extends Model
{
    /**
     * @var array
     */
    use SubjectAttribute;
    protected $fillable = ['subject_name','standard_id','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function standard()
    {
        return $this->belongsTo('App\Models\Standard\Standard');
    }
    public function chapters()
    {
        return $this->hasMany('App\Chapter\Chapter','subject_id');
    }
}

