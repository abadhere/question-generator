<?php

Breadcrumbs::register('admin.log-viewer', function ($breadcrumbs) {
    $breadcrumbs->push(__('strings.backend.log-viewer.title'), route('admin.log-viewer'));
});

Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});
Breadcrumbs::register('admin.standard.index', function ($breadcrumbs) {
    $breadcrumbs->push(__('strings.backend.standard.title'), route('admin.standard.index'));
});



require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
