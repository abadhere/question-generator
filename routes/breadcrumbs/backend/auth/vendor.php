<?php
Breadcrumbs::register('admin.vendor.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.auth.user.index');
    $breadcrumbs->push(__('menus.backend.access.vendors.index'), route('admin.vendor.index'));
});
Breadcrumbs::register('admin.vendor.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.auth.user.index');
    $breadcrumbs->push(__('labels.backend.access.vendors.create'), route('admin.vendor.create'));
});