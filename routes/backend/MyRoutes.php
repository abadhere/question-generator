
<?php
Route::group([
    'middleware' => 'role:administrator',
], function () {


    /*
     * Admin Management
     */
    Route::group(['namespace' => 'Excel'], function () {

        Route::get('excel/create', 'ExcelController@create')->name('excel.create');
        Route::post('excel/create', 'ExcelController@store')->name('excel.create');
    
     });
    Route::group(['namespace' => 'Standard'], function () {
        
        Route::resource('standard', 'StandardController');
    });
    
    Route::group(['namespace' => 'Subject'], function () {
        
        Route::resource('subject', 'SubjectController');
    });


  

    Route::group(['namespace' => 'Chapter'], function () {
        
        Route::resource('chapter', 'ChapterController');
    });

    Route::group(['namespace' => 'Videos'], function () {
        
        Route::resource('videos', 'VideosController');
    });

    

    Route::group(['namespace' => 'Question'], function () {
        
        Route::resource('question', 'QuestionController');
        Route::get('/subjects/ajax/{standard_id}','QuestionController@subjectsdata');
        Route::get('/chapters/ajax/{subject_id}','QuestionController@chaptersdata');
        Route::get('/questions/ajax/{chapter_id}','QuestionController@questionsdata');
        Route::get('/videos/ajax/{chapter_id}','QuestionController@videosdata');
        Route::get('/video/ajax/{uid}','QuestionController@uniqvideo');
        Route::get('/qdata/ajax/{q_id}','QuestionController@qdata');
        Route::get('/insert/ajax/','QuestionController@insertdata');
        // Route::get('/search/ajax/{term}','QuestionController@searchdata');
    });
    // Route::group(['namespace' => 'Videos'], function () {
        
    //     Route::get('videos', 'VideosController@index');

    //     Route::post('videos', 'VideosController@search');
    // });


});
