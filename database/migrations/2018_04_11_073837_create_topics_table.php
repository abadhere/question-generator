<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('topic_name');
            $table->integer('standard_id')->unsigned();
            $table->foreign('standard_id')
            ->references('id')->on('standard');
            
            $table->integer('subject_id')->unsigned();
            $table->foreign('subject_id')
            ->references('id')->on('subjects');
           
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}
